import {LitElement, html, css} from 'lit';

export class InputRealTime extends LitElement {
  static get styles() {
    return css`
      #contenedor {
        display: flex;
       justify-content:space-around;
      }
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
    `;
  }

  static get properties() {
    return {
       valor: {type: String},
       color: {type: String}
    };
  }

  constructor() {
    super();
    this.color="black"
  }

  render() {
    return html`
    <div id="contenedor">
    <div style="width:100%">
    <h5>Ingrese texto y elija el color.</h5>
      <input type="text" placeholder="ingrese texto" @input=${this._handleChange}/>
      <div>
          <label>Rojo</label>
          <input type="radio" name="colores" @click=${this._changeColor} value="red"/>
          <label>Azul</label>
          <input type="radio" name="colores" @click=${this._changeColor} value="blue"/>
          <label>Amarillo</label>
          <input type="radio" name="colores" @click=${this._changeColor} value="yellow"/>
      </div>
  </div> 
  <div style="width:100%">
       <h1 style="color:${this.color}">${this.valor}</h1>
  </div>
  </div>
    `;
  }

  _handleChange(e) {
      this.valor = e.target.value
    
  }

  _changeColor(e){
   this.color = e.target.value

  }
}

window.customElements.define('input-real-time', InputRealTime);
